// https://stanfordnlp.github.io/CoreNLP/api.html 
//	https://stanfordnlp.github.io/CoreNLP/coref.html

// How to run coreNLP efficiently, dos and don'ts, brief overview about memory usage and inside of this mechanism.
// https://stanfordnlp.github.io/CoreNLP/memory-time.html
package main;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import edu.stanford.nlp.coref.CorefCoreAnnotations;
import edu.stanford.nlp.coref.data.CorefChain;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;
import edu.stanford.nlp.util.StringUtils;

public class CorefExample {

	// static StanfordCoreNLP pipeline = null;

	static String text = "Joe Smith was born in California. " +
			"In 2017, he went to Paris, France in the summer. " +
			"His flight left at 3:00pm on July 10th, 2017. " +
			"After eating some escargot for the first time, Joe said, \"That was delicious!\" " +
			" He sent a postcard to his sister Jane Smith. " +
			"After hearing about Joe's trip, Jane decided she might go to France one day.";

	static String text2 = "As advertised. Everything works perfectly, I'm very happy with the camera. "
			+ "As a matter of fact I'm going to buy another one for my 2nd car.\r\n";

	static String text3 = "I love my recorder. Bought it obviously because I have tape cassette's with no way to play them. Spent an AMAZING afternoon, listening to my Mom (who passed away in 2005), tell a 90 minute Christmas story of \"Nicholas\",  to my two daughters, many moons ago.  It was a wonderful afternoon of laughter and tears. I thank you, for providing me, the time to spend an afternoon with my Mom again... :)";
	// + "I am giving the speakers a test drive as I write this review. I am deeply impressed by the sound quality of this little box. The high end is really nice. The bass is somewhat lacking, but, I didn't expect room-shaking bass anyway. These are
	// a keeper!!!";

	public static void main(String[] args) throws Exception {

		Properties props = new Properties();
		props.setProperty("annotators", "tokenize,ssplit,pos,lemma,ner,parse,coref");
		StanfordCoreNLP pipeline = new StanfordCoreNLP(props);

		getCoreferenceResolved(pipeline, text3);
	}

	private static String getRegex(String findRegex) {
		if (StringUtils.isNullOrEmpty(findRegex)) {
			throw new IllegalArgumentException("Arguement is null or empty");
		}
		// \\bHe\\b|\\bhe\\b

		StringBuilder fullRegex = new StringBuilder("\\b");
		fullRegex.append(findRegex.substring(0, 1).toUpperCase());

		fullRegex.append(findRegex.substring(1, findRegex.length()));

		fullRegex.append("\\b|\\b" + findRegex.substring(0, 1).toLowerCase() +
				findRegex.substring(1, findRegex.length()) +
				"\\b");

		return fullRegex.toString();
	}

	/**
	 * It resolves coref by considering all pronoun so give sentence who has their own context.
	 *
	 * @param pipeline
	 *            Already initialized StanfordCoreNLP with desired properties.
	 * @param text
	 *            text for which coref is needed to be resolved.
	 */
	public static String getCoreferenceResolved(StanfordCoreNLP pipeline, String text) {

		LocalDateTime startTime = LocalDateTime.now();

		// The outlaw shot the sheriff, but he did not shoot the deputy -> resolved correctly
		// "The outlaw shot the sheriff, but he shot back." -> not correctly
		Annotation document = new Annotation(text);

		// CoreDocument document = new CoreDocument(text3);


		// I don't know which model it is using
		// props.setProperty("coref.algorithm", "neural");

		if (pipeline == null) {
			Properties props = new Properties();
			props.setProperty("annotators", "tokenize,ssplit,pos,lemma,ner,parse,coref");
			pipeline = new StanfordCoreNLP(props);
		}

		pipeline.annotate(document);

		String sentence = "";

		Map<Integer, CorefChain> corefChains = document.get(CorefCoreAnnotations.CorefChainAnnotation.class);
		List<CoreMap> sentences = document.get(CoreAnnotations.SentencesAnnotation.class);

		if (corefChains == null) {
			return null;
		}

		// Map with index of
		Map<Integer, String> sentenceMap = new HashMap<Integer, String>();

		for (int i = 0; i < sentences.size(); i++) {
			sentenceMap.put(i, sentences.get(i).toString());
		}

		for (Map.Entry<Integer, CorefChain> entry : corefChains.entrySet()) {
			System.out.println("Chain " + entry.getKey());
			for (CorefChain.CorefMention m : entry.getValue().getMentionsInTextualOrder()) {
				// We need to subtract one since the indices count from 1 but the Lists start from 0

				System.out.println("Pronoun: " + m.mentionSpan);
				System.out.println("Real noun: " + entry.getValue().getRepresentativeMention().mentionSpan);

				if (sentenceMap.containsKey(m.sentNum - 1) == false) {
					sentenceMap.put(m.sentNum - 1, sentences.get(m.sentNum - 1).toString());
					System.out.println("Sentence index: " + (m.sentNum - 1));
				}

				if (m.mentionSpan.equals(entry.getValue().getRepresentativeMention().mentionSpan) == false) {

					sentence = sentences.get(m.sentNum - 1).toString().replaceAll(getRegex(m.mentionSpan),
							entry.getValue().getRepresentativeMention().mentionSpan);

					sentenceMap.put(m.sentNum - 1, sentence);
				}
			}
		}
		StringBuilder allText = new StringBuilder();
		for (Integer key : sentenceMap.keySet()) {
			// use the key here

			System.out.println(sentenceMap.get(key));
			allText.append(sentenceMap.get(key));

		}

		System.out.println("Coref Time done: " + (Duration.between(startTime, LocalDateTime.now()).toMillis() / 1000));

		return allText.toString();
	}
}



