// http://wiki.languagetool.org/java-api

package main;

import java.io.IOException;
import java.util.List;

import org.languagetool.JLanguageTool;
import org.languagetool.language.AmericanEnglish;
import org.languagetool.rules.RuleMatch;


public class LanguageTool {

	static JLanguageTool langTool = null;

	public static void main(String[] args) {


		// JLanguageTool langTool = new JLanguageTool(new AmericanEnglish());
		// // comment in to use statistical ngram data:
		// // langTool.activateLanguageModelRules(new File("/data/google-ngram-data"));
		// List<RuleMatch> matches;
		// try {
		// matches = langTool.check("A sentence with a ------ error in the Hitchhiker's Guide tot he Galaxy");
		//
		// for (RuleMatch match : matches) {
		// System.out.println("Potential error at characters " +
		// match.getFromPos() + "-" + match.getToPos() + ": " +
		// match.getMessage());
		// System.out.println("Suggested correction(s): " +
		// match.getSuggestedReplacements());
		// }
		// }
		// catch (IOException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		String text = "I lovee this characcter!";
		System.out.println(correctSpelling(text));

	}

	public static String correctSpelling(String text) {

		if (langTool == null) {
			langTool = new JLanguageTool(new AmericanEnglish());
		}

		// comment in to use statistical ngram data:
		// langTool.activateLanguageModelRules(new File("/data/google-ngram-data"));
		List<RuleMatch> matches;
		try {
			matches = langTool.check(text);
			int difference = 0;
			// I am using first suggestion for the replacement
			for (RuleMatch match : matches) {

				if (match.getSuggestedReplacements().size() > 0) {

					String matchWord = match.getSuggestedReplacements().get(0);

					if (matchWord.length() == 1) {
						matchWord = " " + matchWord + " ";
					}

					// text = text.replaceFirst(text.substring(match.getFromPos() + difference, match.getToPos() + difference),
					// match.getSuggestedReplacements().get(0));

					text = text.replaceFirst(text.substring(match.getFromPos() + difference, match.getToPos() + difference),
							matchWord);

					// Need to add difference of index which comes after replacing old match with first suggested word(s).
					difference = matchWord.length() - (match.getToPos() - match.getFromPos());
				}
			}
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return text;
	}
}




