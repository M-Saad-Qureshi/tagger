package main;

import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.w3c.dom.Element;

import com.opencsv.CSVWriter;
import com.sun.xml.internal.ws.db.glassfish.RawAccessorWrapper;

import edu.stanford.nlp.pipeline.AnnotationPipeline;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.semgraph.SemanticGraphCoreAnnotations.CollapsedCCProcessedDependenciesAnnotation;
import edu.stanford.nlp.sentiment.SentimentModel;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;
import edu.stanford.nlp.tagger.maxent.TaggerConfig;
import edu.stanford.nlp.trees.TypedDependency;
import edu.stanford.nlp.util.CoreMap;
import edu.stanford.nlp.util.PropertiesUtils;
import model.CoreMapDependency;
import model.FeatureOpinion;
import model.OpinionAndOrientation;
import model.Sentiment;

public class WordRelationProcessor {

	static SentiWordNetDemoCode sentiwordnet = null;
	static String pathToSWN = "sentiwordnet\\SentiWordNet_3.0.0_20130122.txt";

	// static List<Sentiment> removeUnrelatedRelations(List<Sentiment> sentimentList) {
	//
	// for (Sentiment sentiment : sentimentList) {
	// for (CoreMap sentence : sentiment.getSentences()) {
	// SemanticGraph sg = sentence.get(CollapsedCCProcessedDependenciesAnnotation.class);
	// Collection<TypedDependency> dependencies = sg.typedDependencies();
	// //
	// dependencies.removeIf(b -> !b.reln().toString().contains("amod") || !b.reln().toString().contains("nsubj") ||
	// !b.reln().toString().contains("nmod") || !b.reln().toString().contains("advmods"));
	// }
	// }
	//
	// return sentimentList;
	// }

	public static void main(String[] args) {

		String readFile = "datanew1 - with negation.csv";
		String savingFile = "preprocessed.csv";
		System.out.println("Reading CSV");

		LocalDateTime startTime = LocalDateTime.now();

		Reader reader = null;

		StringBuilder allReviews = new StringBuilder();
		String[] rawReviews = null;
		try {

			reader = new FileReader(readFile);
			CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT);

			int headerIndex = 0;
			for (CSVRecord csvRecord : csvParser) {

				// to skip header
				if (headerIndex > 0) {
					allReviews.append(csvRecord.get(4) + "<->");
				}
				headerIndex++;
			}

			reader.close();
			csvParser.close();

			System.out.println("Reading CSV complete");


			System.out.println("Going for spelling correction");

			// String _correctedReview = LanguageTool.correctSpelling(allReviews.toString());

			// for adding disintegrated reviews
			// rawReviews = _correctedReview.split("<->");
			rawReviews = allReviews.toString().split("<->");

			System.out.println("Spelling correction done");

		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Initializing of objects for coreference

		Properties props = new Properties();
		// Coref step 0.5
		// props.setProperty("annotators", "tokenize,ssplit,pos,lemma,ner,parse,coref");
		props.setProperty("annotators", "tokenize,ssplit,pos");

		StanfordCoreNLP pipeline = new StanfordCoreNLP(props);


		// List<String> reviewsCoreffed = new ArrayList<String>();

		// I commented coref as I have 2 problems with this:
		// 1) Doesn't see any effect of pronoun vs proper noun
		// 2) It removes white space after full stop which makes problem in POS tagging as pos tagger
		// doesn't separate sentence on full stop with no white space.
		// Coref step 1
		// for (int i = 0; i < correctedReviews.length; i++) {
		// reviewsCoreffed.add(CorefExample.getCoreferenceResolved(pipeline, correctedReviews[i]));
		// }

		System.out.println("Going for POS");


		// Initializing of objects for POS tagging
		String[] arguements = { "-model", "models/english-left3words-distsim.tagger", "-outputFormat", "xml" };
		TaggerConfig config = new TaggerConfig(arguements);

		MaxentTagger tagger = new MaxentTagger(config.getModel(), config);

		List<String> _reviewsTaggedList = new ArrayList<String>();
		for (int i = 0; i < rawReviews.length; i++) {
			_reviewsTaggedList.add(Main.getPosTagged(pipeline, tagger, rawReviews[i], true));
		}

		System.out.println("POS tagging done, going for spltting.");

		String LFRemoved = "";
		List<String> reviewsSplitted = new ArrayList<String>();
		for (int i = 0; i < _reviewsTaggedList.size(); i++) {

			LFRemoved = Main.splitNodesOnCC(_reviewsTaggedList.get(i), "<->");
			// After getting split they have new line inserted at the end of xml
			LFRemoved = LFRemoved.replaceAll("\n", "");
			reviewsSplitted.add(LFRemoved);
		}

		List<Sentiment> sentimentList = new ArrayList<Sentiment>();
		for (int i = 0; i < _reviewsTaggedList.size(); i++) {

			LFRemoved = Main.splitNodesOnCC(_reviewsTaggedList.get(i), "<->");
			// After getting split they have new line inserted at the end of xml
			LFRemoved = LFRemoved.replaceAll("\n", "");
			sentimentList.add(new Sentiment());
			sentimentList.get(i).setSentenceString(LFRemoved);
		}


		Properties properties = PropertiesUtils.asProperties(
				"annotators", "tokenize,ssplit,pos,lemma,depparse");
		properties.setProperty("depparse.language", "English");

		// finding word relations

		System.out.println("Going for Finding word relations");

		AnnotationPipeline annotationPipeline = new StanfordCoreNLP(properties);

		for (int i = 0; i < sentimentList.size(); i++) {
			// getting splitted sentences
			sentimentList.get(i).setSentences(StandfordDependency.processWordRelation(annotationPipeline, rawReviews[i]));
			sentimentList.get(i).modfiedSentences = StandfordDependency.removeUnrelatedRelations(sentimentList.get(i).getSentences());
			sentimentList.get(i).totalWords = rawReviews[i].split("\\s").length;
		}

		sentimentList = processFeatureFrequency(sentimentList, featureCountInAllSentence, rawReviews);
		sentimentList = processTFIDF(sentimentList, featureCountInAllSentence, rawReviews.length);


		for (Sentiment sentiment : sentimentList) {
			sentiment = processSentiment(sentiment);
		}

		for (Sentiment sentiment : sentimentList) {

			for (Map.Entry<String, FeatureOpinion> featureOpinionEntery : sentiment.featureOpinionMap.entrySet()) {
				String feature = featureOpinionEntery.getKey();
				FeatureOpinion featureOpinion = featureOpinionEntery.getValue();

				System.out.println("Feature: " + feature + " TF: " + featureOpinion.termFrequency + " | " + "IDF: " + featureOpinion.IDF +
						" | " + "TF x IDF: " + featureOpinion.TFIDF);
				featureOpinion.opinions.stream().forEach(o -> System.out.println("Negation: " + o.hasNegation + " - Opinion: " + o.opinion
						+ " - Score: " + o.orientationScore));
			}
		}


		System.out.println("Spltting done. Going for writng into CSV");
		/**
		 * CSVWriter writer = null;
		 * Reader reader2 = null;
		 * String[] completeLine = new String[16];
		 * 
		 * try {
		 * 
		 * completeLine[15] = "reviews_tagged";
		 * 
		 * writer = new CSVWriter(new OutputStreamWriter(new FileOutputStream(savingFile), "UTF-8"),
		 * '\t', CSVWriter.NO_QUOTE_CHARACTER, CSVWriter.NO_ESCAPE_CHARACTER);
		 * reader2 = new FileReader(readFile);
		 * CSVFormat csvFormat = CSVFormat.DEFAULT.withQuote('"').withQuote(' ').withDelimiter('\t');
		 * 
		 * CSVParser csvParser = new CSVParser(reader2, csvFormat);
		 * 
		 * int reviewIndex = -1;
		 * for (CSVRecord csvRecord : csvParser) {
		 * 
		 * for (int j = 0; j < csvRecord.size(); j++) {
		 * // to skip header
		 * completeLine[j] = csvRecord.get(j);
		 * }
		 * // to avoid header
		 * if (reviewIndex >= 0) {
		 * // csvRecord.size() should be (reviewsSplitted.size() + 1)
		 * 
		 * completeLine[15] = reviewsSplitted.get(reviewIndex);
		 * }
		 * 
		 * reviewIndex++;
		 * 
		 * writer.writeNext(completeLine);
		 * }
		 * writer.flush();
		 * reader2.close();
		 * csvParser.close();
		 * writer.close();
		 * 
		 * }
		 * catch (IOException e) {
		 * // TODO Auto-generated catch block
		 * e.printStackTrace();
		 * }
		 * 
		 **/

		System.out.println("All process time done: " + (Duration.between(startTime, LocalDateTime.now()).toMillis() / 1000));
	}

	/**
	 * It contains all features with their total count in all documents
	 */
	public static Map<String, Integer> featureCountInAllSentence = new HashMap<String, Integer>();

	/**
	 * Add one count in a featureMap, which indicates total count of feature in all documents.
	 * This map can be used for calculating IDF
	 * 
	 * @param featureMap
	 *            Map in which feature count is needed to be added.
	 * @param feature
	 *            Feature against which count is added.
	 */
	private static void addFeatureInMap(Map<String, Integer> featureMap, String feature) {

		Integer value = featureMap.get(feature);

		if (value == null) {
			value = 0;
		}

		featureMap.put(feature, value + 1);
	}

	/**
	 * It finds feature and opinion by analyzing dependency type and count the frequency of feature.
	 * It also fills all count of features appearance in all documents.
	 * It detects these dependencies: amod, advmods, nmod, nsubj
	 * 
	 * @param sentimentList
	 *            List of sentiment with their dependencies
	 * @param rawReviews
	 *            Reviews which are read from data file.
	 * @return Returns the list of sentiment with processed features, opinions and feature count.
	 */
	private static List<Sentiment> processFeatureFrequency(List<Sentiment> sentimentList, Map<String, Integer> featureFreqInAllSentence,
			String[] rawReviews) {
		// size of sentimentList and rawReviews should be equal
		Integer wordCount = 0;
		for (int i = 0; i < sentimentList.size(); i++) {

			for (CoreMapDependency sentence : sentimentList.get(i).modfiedSentences) {

				String negationWord = "";
				boolean negationFound = false;
				int opinionPosition = -1;
				int negationPosition = -1;

				String feature = null;

				for (int j = 0; j < sentence.dependencies.size(); j++) {
					// dependencies.removeIf(b -> !b.reln().toString().contains("amod") || !b.reln().toString().contains("nsubj") ||
					// !b.reln().toString().contains("nmod") || !b.reln().toString().contains("advmods"));
					FeatureOpinion featureOpinion = null;
					// String feature = null;
					String opinion = null;

					if (sentence.dependencies.get(j).reln().toString().equals("amod")) {

						feature = sentence.dependencies.get(j).gov().toString().toLowerCase().split("/")[0];
						opinion = sentence.dependencies.get(j).dep().toString();

						wordCount = countWordFrequency(rawReviews[i], feature);
						featureOpinion = sentimentList.get(i).featureOpinionMap.get(feature);
						opinionPosition = extractWordPosition(sentence.dependencies.get(j).toString(), 1);
					}

					else if (sentence.dependencies.get(j).reln().toString().equals("advmod")) {

						feature = sentence.dependencies.get(j).gov().toString().toLowerCase().split("/")[0];
						opinion = sentence.dependencies.get(j).dep().toString();

						wordCount = countWordFrequency(rawReviews[i], feature);
						featureOpinion = sentimentList.get(i).featureOpinionMap.get(feature);
						opinionPosition = extractWordPosition(sentence.dependencies.get(j).toString(), 1);
					}

					else if (sentence.dependencies.get(j).reln().toString().equals("nmod")) {

						feature = sentence.dependencies.get(j).gov().toString().toLowerCase().split("/")[0];
						opinion = sentence.dependencies.get(j).dep().toString();

						wordCount = countWordFrequency(rawReviews[i], feature);
						featureOpinion = sentimentList.get(i).featureOpinionMap.get(feature);
						opinionPosition = extractWordPosition(sentence.dependencies.get(j).toString(), 1);
					}

					else if (sentence.dependencies.get(j).reln().toString().equals("nsubj")) {

						opinion = sentence.dependencies.get(j).gov().toString();
						feature = sentence.dependencies.get(j).dep().toString().toLowerCase().split("/")[0];

						wordCount = countWordFrequency(rawReviews[i], feature);
						featureOpinion = sentimentList.get(i).featureOpinionMap.get(feature);
						opinionPosition = extractWordPosition(sentence.dependencies.get(j).toString(), 0);
					}

					else if (sentence.dependencies.get(j).reln().toString().equals("neg")) {

						negationFound = true;
						opinion = sentence.dependencies.get(j).gov().toString();
						negationWord = sentence.dependencies.get(j).dep().toString().toLowerCase().split("/")[0];
						opinionPosition = extractWordPosition(sentence.dependencies.get(j).toString(), 0);
						negationPosition = extractWordPosition(sentence.dependencies.get(j).toString(), 1);

						// We want to skip adding of feature till last iteration
						// because till that time feature would have been found
						if (j != sentence.dependencies.size() - 1) {
							continue;
						}

						// else if(feature == null)
						// {
						// continue;
						// }
					}

					// Further conditions will come for remaining dependencies

					if (featureOpinion == null) {
						featureOpinion = new FeatureOpinion();
					}

					OpinionAndOrientation opnionAndOrientation = new OpinionAndOrientation();

					if (j == sentence.dependencies.size() - 1) {
						opnionAndOrientation.hasNegation = negationFound;
						opnionAndOrientation.negationWord = negationWord;
						opnionAndOrientation.negationPosition = negationPosition;
						opnionAndOrientation.opinionPosition = opinionPosition;
					}

					opnionAndOrientation.opinion = opinion.split("/")[0];
					opnionAndOrientation.opinionPOS = opinion.split("/")[1];
					opnionAndOrientation.opinionPosition = opinionPosition;
					featureOpinion.count = wordCount;
					featureOpinion.opinions.add(opnionAndOrientation);

					sentimentList.get(i).featureOpinionMap.put(feature, featureOpinion);

					addFeatureInMap(featureFreqInAllSentence, feature);
				}

				negationFound = false;
			}
		}

		return sentimentList;
	}

	/**
	 * Calculate TF and IDF of all features
	 * 
	 * @param sentimentList
	 *            List containing sentiment
	 * @param featureCountInAllDocMap
	 *            Map containing all features with total number of doc in which they appear.
	 * @param totalDocument
	 *            Number of total documents
	 * @return List of sentiment after processing.
	 */
	private static List<Sentiment> processTFIDF(List<Sentiment> sentimentList,
			Map<String, Integer> featureCountInAllDocMap, int totalDocument) {

		for (int i = 0; i < sentimentList.size(); i++) {

			for (Map.Entry<String, FeatureOpinion> featureOpinionEntery : sentimentList.get(i).featureOpinionMap.entrySet()) {
				String feature = featureOpinionEntery.getKey();
				FeatureOpinion featureOpinion = featureOpinionEntery.getValue();

				// casting one number to double is necessary for getting double result.
				featureOpinion.termFrequency = featureOpinion.count / (double) sentimentList.get(i).totalWords;

				int _featureCountInAllDoc = featureCountInAllDocMap.get(feature);
				featureOpinion.IDF = Math.log10((double) totalDocument / _featureCountInAllDoc);
				featureOpinion.TFIDF = featureOpinion.termFrequency * featureOpinion.IDF;

				sentimentList.get(i).featureOpinionMap.put(feature, featureOpinion);
			}
		}

		return sentimentList;
	}

	/**
	 * It counts the number of appearance of given word in a given sentence.
	 * 
	 * @param sentence
	 *            Sentence in which word frequency will be counted.
	 * @param word
	 *            Term to find in sentence.
	 * @return Number of appearance of given word in a given sentence
	 */
	private static Integer countWordFrequency(String sentence, String word) {
		// took from https://stackoverflow.com/questions/8938498/get-the-index-of-a-pattern-in-a-string-using-rege

		Integer wordCount = 0;
		String _regex = "\\b" + word + "\\b";

		Pattern pattern = Pattern.compile(_regex);
		Matcher matcher = pattern.matcher(sentence);
		// Check all occurrences
		while (matcher.find()) {
			wordCount += 1;
		}

		return wordCount;
	}

	/**
	 * It extracts the position number of a word which is written with it in relation.
	 * 
	 * @param sentence
	 *            Sentence with words and their position
	 * @param whichMatch
	 *            For which word (index is zero based)
	 * @return word position number
	 */
	private static Integer extractWordPosition(String sentence, int whichMatch) {
		// https://stackoverflow.com/questions/2367381/how-to-extract-numbers-from-a-string-and-get-an-array-of-ints/2367418
		Integer wordPosition = 0;
		System.out.println("Going to extract word position: " + sentence);

		sentence = sentence.replaceAll("[^0-9]+", " ");

		System.out.println("Going to extract word position: " + sentence);
		wordPosition = Integer.valueOf(sentence.trim().split(" ")[whichMatch]);

		return wordPosition;
	}


	private static Sentiment processSentiment(Sentiment sentiment) {

		if (sentiwordnet == null) {
			try {
				sentiwordnet = new SentiWordNetDemoCode(pathToSWN);
			}
			catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		String pos = "";

		for (Map.Entry<String, FeatureOpinion> featureOpinionEntery : sentiment.featureOpinionMap.entrySet()) {
			String feature = featureOpinionEntery.getKey();
			FeatureOpinion featureOpinion = featureOpinionEntery.getValue();

			for (OpinionAndOrientation _opinion : featureOpinion.opinions) {
				if (_opinion.hasNegation) {
					_opinion.orientationScore = -1;
				}

				if (_opinion.opinionPOS.equals("JJ")) {
					pos = "a";
				}

				else if (_opinion.opinionPOS.equals("RB")) {
					pos = "r";
				}

				else if (_opinion.opinionPOS.equals("VBN")) {
					pos = "v";
				}

				_opinion.orientationScore += sentiwordnet.extract(_opinion.opinion, pos);
			}

		}

		return sentiment;
	}

}
