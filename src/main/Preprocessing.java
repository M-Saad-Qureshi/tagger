/*
 * I am not using this class anymore. It was my very first experiment for my thesis.
 */

package main;

import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import com.opencsv.CSVWriter;

import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;
import edu.stanford.nlp.tagger.maxent.TaggerConfig;

public class Preprocessing {

	public static void main(String[] args) {

		String readFile = "data1.tsv";
		String savingFile = "preprocessed.tsv";
		System.out.println("Reading TSV");

		LocalDateTime startTime = LocalDateTime.now();

		Reader reader = null;

		StringBuilder allReviews = new StringBuilder();
		String[] reviews = null;
		try {

			reader = new FileReader(readFile);
			CSVParser csvParser = new CSVParser(reader, CSVFormat.newFormat('\t'));

			int headerIndex = 0;
			for (CSVRecord csvRecord : csvParser) {

				// to skip header
				if (headerIndex > 0) {
					allReviews.append(csvRecord.get(13) + "<->");
				}
				headerIndex++;
			}

			reader.close();
			csvParser.close();

			System.out.println("Reading TSV complete");


			System.out.println("Going for spelling correction");

			// String _correctedReview = LanguageTool.correctSpelling(allReviews.toString());

			// for adding disintegrated reviews
			// reviews = _correctedReview.split("<->");
			reviews = allReviews.toString().split("<->");

			System.out.println("Spelling correction done");

		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Initializing of objects for coreference

		Properties props = new Properties();
		// Coref step 0.5
		// props.setProperty("annotators", "tokenize,ssplit,pos,lemma,ner,parse,coref");
		props.setProperty("annotators", "tokenize,ssplit,pos");

		StanfordCoreNLP pipeline = new StanfordCoreNLP(props);


		// List<String> reviewsCoreffed = new ArrayList<String>();

		// I commented coref as I have 2 problems with this:
		// 1) Doesn't see any effect of pronoun vs proper noun
		// 2) It removes white space after full stop which makes problem in POS tagging as pos tagger
		// doesn't separate sentence on full stop with no white space.
		// Coref step 1
		// for (int i = 0; i < correctedReviews.length; i++) {
		// reviewsCoreffed.add(CorefExample.getCoreferenceResolved(pipeline, correctedReviews[i]));
		// }

		System.out.println("Going for POS");


		// Initializing of objects for POS tagging
		String[] arguements = { "-model", "models/english-left3words-distsim.tagger", "-outputFormat", "xml" };
		TaggerConfig config = new TaggerConfig(arguements);

		MaxentTagger tagger = new MaxentTagger(config.getModel(), config);

		List<String> reviewsTaggedList = new ArrayList<String>();
		for (int i = 0; i < reviews.length; i++) {
			reviewsTaggedList.add(Main.getPosTagged(pipeline, tagger, reviews[i], true));
		}

		System.out.println("POS tagging done, going for spltting.");

		String LFRemoved = "";
		List<String> reviewsSplitted = new ArrayList<String>();
		for (int i = 0; i < reviewsTaggedList.size(); i++) {

			LFRemoved = Main.splitNodesOnCC(reviewsTaggedList.get(i), "<->");
			LFRemoved = LFRemoved.replaceAll("\n", "");
			reviewsSplitted.add(LFRemoved);
		}

		System.out.println("Spltting done. Going for writng into CSV");

		CSVWriter writer = null;
		Reader reader2 = null;
		String[] completeLine = new String[16];

		try {

			completeLine[15] = "reviews_tagged";

			// writer = new CSVWriter(new FileWriter(savingFile,""), '\t',CSVWriter.NO_QUOTE_CHARACTER);
			writer = new CSVWriter(new OutputStreamWriter(new FileOutputStream(savingFile), "UTF-8"),
					'\t', CSVWriter.NO_QUOTE_CHARACTER, CSVWriter.NO_ESCAPE_CHARACTER);
			reader2 = new FileReader(readFile);
			// CSVFormat csvFormat = CSVFormat.RFC4180.withFirstRecordAsHeader()
			// .withIgnoreEmptyLines(true)
			// .withTrim().withDelimiter('\t').withQuote(null);
			// CSVFormat csvFormat = CSVFormat.newFormat('\t');
			CSVFormat csvFormat = CSVFormat.DEFAULT.withQuote('"').withQuote(' ').withDelimiter('\t');

			CSVParser csvParser = new CSVParser(reader2, csvFormat);

			int reviewIndex = -1;
			for (CSVRecord csvRecord : csvParser) {

				for (int j = 0; j < csvRecord.size(); j++) {
					// to skip header
					completeLine[j] = csvRecord.get(j);
				}
				// to avoid header
				if (reviewIndex >= 0) {
					// csvRecord.size() should be (reviewsSplitted.size() + 1)

					completeLine[15] = reviewsSplitted.get(reviewIndex);
				}

				reviewIndex++;

				writer.writeNext(completeLine);
			}
			writer.flush();
			reader2.close();
			csvParser.close();
			writer.close();

		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("All process time done: " + (Duration.between(startTime, LocalDateTime.now()).toMillis() / 1000));
	}

}
