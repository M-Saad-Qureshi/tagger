/*
 * I am not using this class anymore. It was my very first experiment for my thesis.
 */

package main;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

import com.opencsv.CSVWriter;

import model.Sentiment;

public class SentimentFeatureAnalysis {

	public static void main(String[] args) {

		String readFile = "preprocessed.tsv";
		String savingFile = "sentimentdone.tsv";

		LocalDateTime startTime = LocalDateTime.now();

		Reader reader = null;

		List<String> reviews = new ArrayList<String>();

		try {

			reader = new FileReader(readFile);
			CSVParser csvParser = new CSVParser(reader, CSVFormat.newFormat('\t'));

			int headerIndex = 0;
			for (CSVRecord csvRecord : csvParser) {

				// to skip header
				if (headerIndex > 0) {
					reviews.add(csvRecord.get(15));
				}
				headerIndex++;
			}

			// reader = new CSVReader(new FileReader(readFile), '\t');
			// String[] nextLine;
			// // to escpae headers
			// reader.readNext();
			// StringBuilder allReviews = new StringBuilder();
			//
			// while ((nextLine = reader.readNext()) != null) {
			// // nextLine[13] is with review body
			// reviews.add(nextLine[15]);
			// }
			reader.close();
			csvParser.close();

		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String[] splittedReview = null;
		List<Sentiment> sentimientList = new ArrayList<Sentiment>();
		Sentiment sentiment = null;
		Sentiment tempSentiment = null;

		// try {

		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = null;
		try {
			dBuilder = dbFactory.newDocumentBuilder();
		}
		catch (ParserConfigurationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Document doc = null;
		Node sentenceNode = null;

		for (int i = 0; i < reviews.size(); i++) {
			splittedReview = reviews.get(i).split("<->");
			tempSentiment = new Sentiment();
			
			try {
				for (int j = 0; j < splittedReview.length; j++) {
					doc = dBuilder.parse(new InputSource(new StringReader(splittedReview[j])));
					doc.getDocumentElement().normalize();

					// there should be only 1 sentence
					sentenceNode = doc.getElementsByTagName("sentence").item(0);

					sentiment = Main.getSentiment(sentenceNode);

					tempSentiment.setSentenceString(tempSentiment.getSentenceString() + "<->" + sentiment.getSentenceString());
					tempSentiment.setScore(tempSentiment.getScore() + sentiment.getScore());
					tempSentiment.setFeature(tempSentiment.getFeature() + "<->" + sentiment.getFeature());
					tempSentiment.setOpinion(tempSentiment.getOpinion() + "<->" + sentiment.getOpinion());
				}
			}

			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			sentimientList.add(tempSentiment);

		}
		// }

		// catch (Exception e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }

		CSVWriter writer = null;
		Reader reader2 = null;
		String[] completeLine = new String[19];

		try {

			completeLine[16] = "score";
			completeLine[17] = "feature";
			completeLine[18] = "opinion";

			writer = new CSVWriter(new FileWriter(savingFile), '\t');
			reader2 = new FileReader(readFile);
			CSVParser csvParser = new CSVParser(reader2, CSVFormat.newFormat('\t'));

			int reviewIndex = -1;
			for (CSVRecord csvRecord : csvParser) {

				for (int j = 0; j < csvRecord.size(); j++) {
					// to skip header
					completeLine[j] = csvRecord.get(j);
				}
				// to avoid header
				if (reviewIndex >= 0 && reviewIndex < sentimientList.size()) {
					// csvRecord.size() should be (reviewsSplitted.size() + 1)

					completeLine[16] = String.valueOf(sentimientList.get(reviewIndex).getScore());
					completeLine[17] = sentimientList.get(reviewIndex).getFeature();
					completeLine[18] = sentimientList.get(reviewIndex).getOpinion();
				}

				reviewIndex++;

				writer.writeNext(completeLine);
			}

			reader2.close();
			csvParser.close();
			writer.close();
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// for (Sentiment senti : sentimientList) {
		// System.out.println("Score: " + senti.getScore());
		// System.out.println("Opinion & Feature: (" + senti.getOpinion() +
		// " , " + senti.getFeature() + ")");
		//
		// System.out.println("Sentence: " + senti.getSentenceString());
		// }


	}

}
