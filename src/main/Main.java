package main;

import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.InputSource;

import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;
import edu.stanford.nlp.tagger.maxent.TaggerConfig;
import model.Sentiment;

public class Main {

	static SentiWordNetDemoCode sentiwordnet = null;
	static String pathToSWN = "sentiwordnet\\SentiWordNet_3.0.0_20130122.txt";
	static StanfordCoreNLP pipeline = null;

	/**
	 * It splits nodes (Sentences) on CC like "and", "but". It give id to the sentences as well.
	 * Children sentence will have id like "parentId.childId"
	 * 
	 * @param nodeList
	 * @return
	 */
	public static List<Node> splitNodesOnCC(NodeList nodeList) {
		List<Node> m_nodeList = new ArrayList<Node>();

		DocumentBuilderFactory icFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder icBuilder;
		Document doc = null;

		try {
			icBuilder = icFactory.newDocumentBuilder();
			doc = icBuilder.newDocument();
		}
		catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for (int i = 0; i < nodeList.getLength(); i++) {
			Element sentenceElement = doc.createElement("sentence");
			Element sen = (Element) nodeList.item(i);
			NodeList wordList = sen.getElementsByTagName("word");
			sentenceElement.setAttribute("id", sen.getAttribute("id"));

			m_nodeList.add(sentenceElement);
			int childId = 1;

			for (int j = 0; j < wordList.getLength(); j++) {
				Element wordElement = (Element) wordList.item(j);

				if (wordElement.getAttribute("pos").equals("CC") &&
						wordElement.getTextContent().toLowerCase().trim().equals("or") == false) {

					Element sentenceElement2 = doc.createElement("sentence");
					sentenceElement2.setAttribute("id", sen.getAttribute("id") + "." + childId++);

					int j2 = j;
					Node imported;
					do {
						imported = doc.importNode(wordElement, true);
						sentenceElement2.appendChild(imported);
						j2 += 1;
						if (j2 < wordList.getLength()) {
							wordElement = (Element) wordList.item(j2);
						}
					}
					while ((wordElement.getAttribute("pos").equals("CC") == false ||
							wordElement.getTextContent().toLowerCase().trim().equals("or") == true)
							&& j2 < wordList.getLength());

					j = j2 - 1;
					m_nodeList.add(sentenceElement2);
				}

				else {
					Node imported = doc.importNode(wordElement, true);
					sentenceElement.appendChild(imported);
				}
			}

		}

		return m_nodeList;
	}

	/**
	 * It splits nodes (Sentences) on CC like "and", "but". It give id to the sentences as well.
	 * Children sentence will have id like "parentId.childId"
	 * @param xmlDocument
	 * @param seperator it will add seperator between multiple sentence tag
	 * @return
	 */
	public static String splitNodesOnCC(String xmlDocument, String seperator) {

		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder;
		NodeList nList = null;
		List<Node> m_nodeList = new ArrayList<Node>();
		StringBuilder allReviews = new StringBuilder();

		try {
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(new InputSource(new StringReader(xmlDocument)));
			doc.getDocumentElement().normalize();
			nList = doc.getElementsByTagName("sentence");

			m_nodeList = splitNodesOnCC(nList);

			for (int i = 0; i < m_nodeList.size(); i++) {

				allReviews.append(getStringXML(m_nodeList.get(i)) + seperator);
			}
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return allReviews.toString();
	}

	public static String getStringXML(Node node) {
		Document document = node.getOwnerDocument();
		DOMImplementationLS domImplLS = (DOMImplementationLS) document
				.getImplementation();
		LSSerializer serializer = domImplLS.createLSSerializer();
		return serializer.writeToString(node);
	}

	private static List<Sentiment> getSentiment(List<Node> sentenceList) {

		List<Sentiment> sentimentList = new ArrayList<>();

		// It will run on each sentence and each sentence should
		// have atleast one opinion word and a feature word.

		boolean opinionFound = false;
		boolean featureFound = false;
		boolean negationFound = false;

		Sentiment sentiment;
		Element sentenceElement;
		double score = 0.0;
		String opinion = "";
		String feature = "";

		for (int i = 0; i < sentenceList.size(); i++) {

			// getting sentence to read each word
			sentenceElement = (Element) sentenceList.get(i);

			for (int j = 0; j < sentenceElement.getChildNodes().getLength(); j++) {

				if (haveNegation((Element) sentenceElement.getChildNodes().item(j))) {
					negationFound = true;
				}

				if (haveOpinion((Element) sentenceElement.getChildNodes().item(j))) {
					opinionFound = true;

					try {
						score = getScore((Element) sentenceElement.getChildNodes().item(j));
					}

					catch (Exception ex) {
						System.out.println("problematic Word: " + getStringXML(sentenceElement));
						opinionFound = false;
						continue;
					}

					opinion = ((Element) sentenceElement.getChildNodes().item(j)).getTextContent();

					if (negationFound) {
						score = (score <= 0) ? score + 1 : score - 1;
					}
				}

				else if (haveFeature((Element) sentenceElement.getChildNodes().item(j))) {
					featureFound = true;
					feature = ((Element) sentenceElement.getChildNodes().item(j)).getTextContent();
				}

				if (opinionFound && featureFound) {
					sentiment = new Sentiment();
					sentiment.setSentence(sentenceElement);
					sentiment.setScore(score);
					sentiment.setFeature(feature);
					sentiment.setOpinion(opinion);

					sentimentList.add(sentiment);

					opinionFound = false;
					featureFound = false;
					negationFound = false;

					break;
				}
			}

			opinionFound = false;
			featureFound = false;
			negationFound = false;
		}

		return sentimentList;
	}

	/**
	 * It analyze the sentence for opinion and feature words and calculate score on opinion.
	 * @param sentence Sentence to analyze
	 * @return Sentiment object with sentence, score, opinion and feature word.
	 */
	public static Sentiment getSentiment(Node sentence) {		

		// It will run on each sentence and each sentence should
		// have atleast one opinion word and a feature word.

		boolean opinionFound = false;
		boolean featureFound = false;
		boolean negationFound = false;

		Sentiment sentiment = new Sentiment();
		Element sentenceElement;
		double score = 0.0;
		String opinion = "";
		String feature = "";

		// getting sentence to read each word
		sentenceElement = (Element) sentence;

		for (int j = 0; j < sentenceElement.getChildNodes().getLength(); j++) {

			if (haveNegation((Element) sentenceElement.getChildNodes().item(j))) {
				negationFound = true;
			}

			if (haveOpinion((Element) sentenceElement.getChildNodes().item(j))) {
				opinionFound = true;

				try {
					score = getScore((Element) sentenceElement.getChildNodes().item(j));
				}

				catch (Exception ex) {
					System.out.println("problematic Word: " + getStringXML(sentenceElement));
					opinionFound = false;
					continue;
				}

				opinion = ((Element) sentenceElement.getChildNodes().item(j)).getTextContent();

				if (negationFound) {
					score = (score <= 0) ? score + 1 : score - 1;
				}
			}

			else if (haveFeature((Element) sentenceElement.getChildNodes().item(j))) {
				featureFound = true;
				feature = ((Element) sentenceElement.getChildNodes().item(j)).getTextContent();
			}

			if (opinionFound && featureFound) {
				
				sentiment.setSentence(sentenceElement);
				sentiment.setScore(score);
				sentiment.setFeature(feature);
				sentiment.setOpinion(opinion);
				sentiment.setSentenceString(getStringXML(sentenceElement));


//				sentimentList.add(sentiment);

				opinionFound = false;
				featureFound = false;
				negationFound = false;

				break;
			}
		}

		return sentiment;
	}

	private static boolean haveNegation(Element wordElement) {
		boolean m_haveNegation = false;
		// negation words: never, not, no, n't (wouldn't)
		Pattern p = Pattern.compile("(\\w+n't)|(not)|(no)|(never)|(n't)");
		Matcher m = p.matcher(wordElement.getTextContent().toLowerCase());
		if (m.find()) {
			m_haveNegation = true;
		}
		return m_haveNegation;
	}

	private static boolean haveOpinion(Element wordElement) {
		boolean m_haveOpinion = false;

		// Adjective , Adverb
		if ((wordElement.getAttribute("pos").equals("JJ") || wordElement.getAttribute("pos").equals("RB")) &&
				wordElement.getTextContent().equalsIgnoreCase("n't") == false
				&& wordElement.getTextContent().equalsIgnoreCase("not") == false
				&& wordElement.getTextContent().equalsIgnoreCase("never") == false) {
			// if it is adverb then it should not "so"
			if (wordElement.getTextContent().equalsIgnoreCase("so") == false) {
				m_haveOpinion = true;
			}
		}

		return m_haveOpinion;
	}

	private static boolean haveFeature(Element wordElement) {
		boolean m_haveFeature = false;

		if (wordElement.getAttribute("pos").equals("NN") || wordElement.getAttribute("pos").equals("NNS")
				|| wordElement.getAttribute("pos").equals("NNP") || wordElement.getAttribute("pos").equals("NNPS")) {
			m_haveFeature = true;
		}

		return m_haveFeature;
	}

	private static double getScore(Element wordElement) {
		if (sentiwordnet == null) {
			try {
				sentiwordnet = new SentiWordNetDemoCode(pathToSWN);
			}
			catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		String pos = "";

		if (wordElement.getAttribute("pos").equals("JJ")) {
			pos = "a";
		}

		else if (wordElement.getAttribute("pos").equals("RB")) {
			pos = "r";
		}

//		System.out.println("Word: " + wordElement.getTextContent().toLowerCase().trim() + "	POS: " + pos);

		return sentiwordnet.extract(wordElement.getTextContent().toLowerCase().trim(), pos);
	}

	/**
	 * It give sample with POS tagged in XML format
	 * 
	 * @param pipeline
	 *            Already initialized StanfordCoreNLP with desired properties.
	 * @param tagger
	 *            Already initialized MaxentTagger with desired arguments.
	 * @param sample
	 *            Text for POS tagging.
	 * @return
	 */
	public static String getPosTagged(StanfordCoreNLP pipeline, MaxentTagger tagger, String sample, boolean includeXMLHeader) {

		String tagged = "";

		if (includeXMLHeader) {
			tagged += "<?xml version=\"1.0\"?>\n<sentences>\n";
		}
		try {

			if (tagger == null) {

				String[] arguements = { "-model", "models/english-left3words-distsim.tagger", "-outputFormat", "xml" };
				TaggerConfig config = new TaggerConfig(arguements);
				// TODO Auto-generated method stub
				// Initialize the tagger
				// MaxentTagger tagger = new MaxentTagger("taggers/models/left3words-distsim-wsj-0-18.tagger");
				tagger = new MaxentTagger(config.getModel(), config);
			}

			if (pipeline == null) {

				// Preparing pipeline for
				Properties props = new Properties();
				props.setProperty("annotators", "tokenize,ssplit,pos");
				pipeline = new StanfordCoreNLP(props);
			}

			tagged += tagger.tagString(sample);

			if (includeXMLHeader) {
				tagged += "</sentences>";
			}
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return tagged;
	}

	public static void main(String[] args) {

		String sample = "";
		try {
			sample = new String(Files.readAllBytes(Paths.get("data.txt")));
		}
		catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		String[] arguements = { "-model", "models/english-left3words-distsim.tagger", "-outputFormat", "xml" };
		TaggerConfig config = new TaggerConfig(arguements);
		// TODO Auto-generated method stub
		// Initialize the tagger
		// MaxentTagger tagger = new MaxentTagger("taggers/models/left3words-distsim-wsj-0-18.tagger");
		MaxentTagger tagger = new MaxentTagger(config.getModel(), config);
		// Preparing pipeline for
		Properties props = new Properties();
		props.setProperty("annotators", "tokenize,ssplit,pos");
		pipeline = new StanfordCoreNLP(props);

		String tagged = getPosTagged(pipeline, tagger, sample, true);

		// Output the result
		System.out.println(tagged);

		try {

			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(new InputSource(new StringReader(tagged)));
			doc.getDocumentElement().normalize();
			NodeList nList = doc.getElementsByTagName("sentence");

			List<Node> nodeList = splitNodesOnCC(nList);

			List<Sentiment> sentimientList = getSentiment(nodeList);

			for (Sentiment sentiment : sentimientList) {
				System.out.println("Score: " + sentiment.getScore());
				System.out.println("Opinion & Feature: (" + sentiment.getOpinion() +
						" , " + sentiment.getFeature() + ")");

				System.out.println("Sentence: " + getStringXML(sentiment.getSentence()) + "\n");
			}
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
