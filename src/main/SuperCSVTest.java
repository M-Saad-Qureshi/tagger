package main;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.FileReader;
import java.io.Reader;

public class SuperCSVTest {

	public static void main(String[] args) {

		
		try {		
		Reader reader = new FileReader("data.tsv");
		CSVParser csvParser = new CSVParser(reader, CSVFormat.newFormat('\t'));
		
			for (CSVRecord csvRecord : csvParser) {
				// Accessing Values by Column Index
				String review = csvRecord.get(13);		

				System.out.println("Record No - " + csvRecord.getRecordNumber());				
				System.out.println("Review : " + review);	
			}
			
			csvParser.close();
		}
		
		catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}

}
