package main;

import edu.stanford.nlp.trees.GrammaticalStructure;
import edu.stanford.nlp.trees.GrammaticalStructureFactory;
import edu.stanford.nlp.trees.PennTreebankLanguagePack;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreebankLanguagePack;
import edu.stanford.nlp.trees.TypedDependency;

import java.util.Collection;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.stanford.nlp.parser.lexparser.*;
import edu.stanford.nlp.ling.SentenceUtils;

public class LexicalParser {

	public void getTree() {

		LexicalizedParser lp = LexicalizedParser.loadModel(
				"edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz",
				"-maxLength", "80", "-retainTmpSubcategories");
		TreebankLanguagePack tlp = new PennTreebankLanguagePack();
		// Uncomment the following line to obtain original Stanford Dependencies
		// tlp.setGenerateOriginalDependencies(true);
		GrammaticalStructureFactory gsf = tlp.grammaticalStructureFactory();
		String[] sent = { "Camera", "is", "never","good", "." };
		Tree parse = lp.apply(SentenceUtils.toWordList(sent));
		GrammaticalStructure gs = gsf.newGrammaticalStructure(parse);
		Collection<TypedDependency> tdl = gs.typedDependenciesEnhancedPlusPlus();

		for (Iterator<TypedDependency> iterator = tdl.iterator(); iterator.hasNext();) {
			TypedDependency type = iterator.next();

			System.out.println(type.toString());
		}

		System.out.println(tdl);
	}
	
	private static boolean haveNegation(String word) {
		boolean m_haveNegation = false;

		Pattern p = Pattern.compile("(\\w+n't)|(not)|(no)|(never)");
		Matcher m = p.matcher(word);
		if (m.find())
		{
			m_haveNegation = true;
		}
		return m_haveNegation;
	}

	public static void main(String[] args) {

		boolean what = haveNegation("Gfive is never good.");
		
		LexicalParser lexParser = new LexicalParser();
		lexParser.getTree();

	}
}
