package main;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.w3c.dom.Element;

import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.parser.nndep.DependencyParser;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.AnnotationPipeline;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.process.DocumentPreprocessor;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.semgraph.SemanticGraphCoreAnnotations.CollapsedCCProcessedDependenciesAnnotation;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;
import edu.stanford.nlp.trees.GrammaticalStructure;
import edu.stanford.nlp.trees.TypedDependency;
import edu.stanford.nlp.util.CoreMap;
import edu.stanford.nlp.util.PropertiesUtils;
import edu.stanford.nlp.util.logging.Redwood;
import model.CoreMapDependency;
import edu.stanford.nlp.ling.HasWord;

public class StandfordDependency {

	/**
	 * It process the text by finding all relations within the text.
	 * 
	 * @param pipeline
	 *            Pipeline for processing
	 * @param text
	 *            Text in which, relations are needed to be found
	 * @return All sentences splitted with ".", with their relations.
	 */
	public static List<CoreMap> processWordRelation(AnnotationPipeline pipeline, String text) {
		// String text = "Bills on ports and immigration were submitted " +
		// "by Senator Brownback, Republican of Kansas";

		Annotation annotation = new Annotation(text);

		if (pipeline == null) {
			Properties properties = PropertiesUtils.asProperties(
					"annotators", "tokenize,ssplit,pos,lemma,depparse");
			properties.setProperty("depparse.language", "English");
			pipeline = new StanfordCoreNLP(properties);
		}

		pipeline.annotate(annotation);
		List<CoreMap> sentences = annotation.get(SentencesAnnotation.class);

		return sentences;
	}

	private static final Redwood.RedwoodChannels log = Redwood.channels(StandfordDependency.class);

	/**
	 * I am not using this method, I see this method for reference purpose.
	 * public static void dependencyParserDemo2(String text) {
	 * // taken from: https://github.com/stanfordnlp/CoreNLP/blob/master/src/edu/stanford/nlp/parser/nndep/demo/DependencyParserDemo.java
	 * String modelPath = DependencyParser.DEFAULT_MODEL;
	 * String taggerPath = "edu/stanford/nlp/models/pos-tagger/english-left3words/english-left3words-distsim.tagger";
	 * // models/english-left3words-distsim.tagger
	 * 
	 * MaxentTagger tagger = new MaxentTagger(taggerPath);
	 * DependencyParser parser = DependencyParser.loadFromModelFile(modelPath);
	 * 
	 * DocumentPreprocessor tokenizer = new DocumentPreprocessor(new StringReader(text));
	 * for (List<HasWord> sentence : tokenizer) {
	 * List<TaggedWord> tagged = tagger.tagSentence(sentence);
	 * GrammaticalStructure gs = parser.predict(tagged);
	 * 
	 * // Print typed dependencies
	 * log.info(gs);
	 * }
	 * }
	 */

	/**
	 * Process splitted sentences and remove unrelated relations
	 * 
	 * @param sentences
	 *            List of splitted sentences (these splitted sentences should be fragments of one complete sentence)
	 * @return List of splitted sentence where each sentences has it reduced dependencies
	 */
	public static List<CoreMapDependency> removeUnrelatedRelations(List<CoreMap> sentences) {
		List<CoreMapDependency> newCoreMapDependencyList = new ArrayList<CoreMapDependency>();
		CoreMapDependency coreMapDependencynew = null;

		for (CoreMap sentence : sentences) {
			SemanticGraph sg = sentence.get(CollapsedCCProcessedDependenciesAnnotation.class);
			Collection<TypedDependency> dependencies = sg.typedDependencies();

			// remove all dependencies except those which are mentioned below
			dependencies.removeIf(b -> !b.reln().toString().equals("amod") && !b.reln().toString().equals("nsubj") &&
					!b.reln().toString().equals("nmod") && !b.reln().toString().equals("advmod")
					&& !b.reln().toString().equals("neg"));

			// dependencies.removeIf(b -> !b.reln().toString().equals("amod") && !b.reln().toString().equals("nsubj") &&
			// !b.reln().toString().equals("nmod") && !b.reln().toString().equals("advmod"));

			Collection<TypedDependency> tempDependencies = new ArrayList<TypedDependency>();

			// remove those dependencies which do not contain feature (noun) & opinion (adjective, adverb, verb 3rd from)
			for (TypedDependency typedDependency : dependencies) {

				if (typedDependency.reln().toString().contains("amod")) {
					if (haveFeature(typedDependency.gov().toString().split("/")[1]) == false ||
							haveOpinion(typedDependency.dep().toString().split("/")[1]) == false) {
						tempDependencies.add(typedDependency);
					}
				}

				else if (typedDependency.reln().toString().contains("nsubj")) {
					if (haveFeature(typedDependency.dep().toString().split("/")[1]) == false ||
							haveOpinion(typedDependency.gov().toString().split("/")[1]) == false) {
						tempDependencies.add(typedDependency);
					}
				}

				else if (typedDependency.reln().toString().contains("nmod")) {
					if (haveFeature(typedDependency.gov().toString().split("/")[1]) == false ||
							haveOpinion(typedDependency.dep().toString().split("/")[1]) == false) {
						tempDependencies.add(typedDependency);
					}
				}

				else if (typedDependency.reln().toString().contains("advmod")) {
					if (haveFeature(typedDependency.gov().toString().split("/")[1]) == false ||
							haveOpinion(typedDependency.dep().toString().split("/")[1]) == false) {
						tempDependencies.add(typedDependency);
					}
				}
				// We need to check if negation relation is really about some opinion
				else if (typedDependency.reln().toString().contains("neg")) {
					// else if (typedDependency.) {
					//
					// }

					if (haveOpinion(typedDependency.gov().toString().split("/")[1]) == false) {
						tempDependencies.add(typedDependency);
					}
				}
			}

			dependencies.removeAll(tempDependencies);

			// Remove that dependency which is only remaining dependency/relation and which is "neg"
			if (dependencies.size() == 1) {
				dependencies.removeIf(b -> b.reln().toString().contains("neg"));
			}
			
			coreMapDependencynew = new CoreMapDependency();
			coreMapDependencynew.sentence = sentence;
			coreMapDependencynew.dependencies = (List<TypedDependency>) dependencies;
			newCoreMapDependencyList.add(coreMapDependencynew);
		}

		return newCoreMapDependencyList;
	}

	private static boolean haveFeature(String wordPOS) {
		boolean m_haveFeature = false;

		if (wordPOS.equals("NN") || wordPOS.equals("NNS")
				|| wordPOS.equals("NNP") || wordPOS.equals("NNPS")) {
			m_haveFeature = true;
		}

		return m_haveFeature;
	}

	private static Integer extractWordPosition(String sentence, int whichMatch) {
		// https://stackoverflow.com/questions/2367381/how-to-extract-numbers-from-a-string-and-get-an-array-of-ints/2367418
		Integer wordPosition = 0;
		sentence = sentence.replaceAll("[^0-9]+", " ");

		wordPosition = Integer.valueOf(sentence.trim().split(" ")[whichMatch]);

		return wordPosition;
	}

	private static boolean haveOpinion(String wordPOS) {
		boolean m_haveOpinion = false;

		// Adjective , Adverb
		if ((wordPOS.equals("JJ") || wordPOS.equals("RB") || wordPOS.equals("VBN"))) {
			m_haveOpinion = true;
		}

		return m_haveOpinion;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String text = "Camera couldn't take good picture. Screen is never good. Video is not good. Sound is of no use.";
		String text2 = "Bought this as an alternative to a smart phone and data plan. I use it with Go-Phone on a pay-as-you-go plan. The phone functions are great. The radio and music players are very good sound quality. The smart phone typing feature is easier and faster to use than any smart phone I have used. There are some software glitches that are documented on the Nokia support site which proved easily fixed. Even though many of the Nokia based features are no longer supported by Microsoft, you can still use the features needed by accessing the web directly via WIFI connection - or data if you want to pay for the privilege - and launching the site/application from there. Although the new Nokia browser is pretty Spartan in appearance, it is fast and runs with good stability. I am a very big guy with lands large even for my size and I find the full qwerty keyboard to be pretty easy to use. I like the programmable hot keys and on-screen shortcuts. Keeps you from launching items that require data connections by accident. The back lighting is thorough and even and the raised buttons are tactile with a positive click response. The phone does have a few quirks. There is no airplane mode as far as I can tell. The flight profile doesn't seem to be able to be set with the broadcasting and receiving functions turned off. The main navigation button/mouse is a little small and doesn't always respond as expected - that might be me though. All in all I would recommend this phone and vendor highly if you want the features of a smart phone but you don't need GPS or a big data bill every month for a bunch of things you don't need or want to keep you life running smoothly.";
		String text3 = "Shipped quickly and was exactly what I expected!";


		Properties properties = PropertiesUtils.asProperties(
				"annotators", "tokenize,ssplit,pos,lemma,depparse");
		properties.setProperty("depparse.language", "English");

		AnnotationPipeline pipeline = new StanfordCoreNLP(properties);

		List<CoreMap> _sentences = processWordRelation(pipeline, text);

		for (CoreMap sentence : _sentences) {
			SemanticGraph sg = sentence.get(CollapsedCCProcessedDependenciesAnnotation.class);
			Collection<TypedDependency> dependencies = sg.typedDependencies();

			for (TypedDependency td : dependencies) {
				System.out.println(td);
				System.out.println("Relation: " + td.reln());
				System.out.println("Governer: " + td.gov());
				System.out.println("Dep: " + td.dep());
				System.out.println(sentence.toString());
			}
		}

		System.out.println("Word position of good: " + extractWordPosition("neg(good-4, not-3)", 1));
	}

}
