package model;

public class OpinionAndOrientation {

	public OpinionAndOrientation() {
		orientationScore = 0;
		opinion = new String();
		opinionPOS = new String();
		negationWord =  new String();
	}

	public double orientationScore;
	public String opinion;
	public String opinionPOS;
	public String negationWord;
	public boolean hasNegation;
	public int opinionPosition;
	public int negationPosition;

}
