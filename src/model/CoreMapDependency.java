package model;

import java.util.List;

import edu.stanford.nlp.trees.TypedDependency;
import edu.stanford.nlp.util.CoreMap;

public class CoreMapDependency {
	
	public CoreMap sentence;
	public List<TypedDependency> dependencies;

	
}
