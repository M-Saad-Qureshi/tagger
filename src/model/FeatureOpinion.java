package model;

import java.util.ArrayList;
import java.util.List;

public class FeatureOpinion {

	public FeatureOpinion() {
		count = 0;
		termFrequency = 0;
		IDF = 0;
		TFIDF = 0;
		opinions = new ArrayList<OpinionAndOrientation>();
	}

	/**
	 * Number of appearance of feature in a sentence
	 */
	public Integer count;
	public double termFrequency;
	public double IDF;
	public double TFIDF;
	public List<OpinionAndOrientation> opinions;

}
