package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Element;

import edu.stanford.nlp.util.CoreMap;

public class Sentiment {

	public Sentiment() {
		score = 0;
		feature = "";
		opinion = "";
		sentenceString = "";
		sentences = new ArrayList<CoreMap>();
		totalWords = 0;
		featureOpinionMap = new HashMap<String, FeatureOpinion>();
	}

	public Element getSentence() {
		return sentence;
	}

	public void setSentence(Element sentence) {
		this.sentence = sentence;
	}

	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}

	public String getFeature() {
		return feature;
	}

	public void setFeature(String feature) {
		this.feature = feature;
	}

	public String getOpinion() {
		return opinion;
	}

	public void setOpinion(String opinion) {
		this.opinion = opinion;
	}

	public String getSentenceString() {
		return sentenceString;
	}

	public void setSentenceString(String sentenceString) {
		this.sentenceString = sentenceString;
	}

	public List<CoreMap> getSentences() {
		return sentences;
	}

	public void setSentences(List<CoreMap> sentences) {
		this.sentences = sentences;
	}

	Element sentence;
	double score;
	String feature;
	String opinion;
	String sentenceString;
	List<CoreMap> sentences;
	
	/**
	 * It contains list of sentences which are splitted on full stop and with unecessary dependencies removed.
	 */
	public List<CoreMapDependency> modfiedSentences;
	public int totalWords;
	
	/**
	 * It contains every feature found in Sentence with its frequency.
	 */
	public Map<String, FeatureOpinion> featureOpinionMap;

}
